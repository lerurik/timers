<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Custom Twitch Timers</title>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i|Indie+Flower|Lato:100,300,400,700|Lobster|Montserrat|Open+Sans:300,400,600,700,800|Oswald|Quicksand|Roboto:100,300,400,500,700,900|Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i%7CRighteous:400%7CRoboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" media="all">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Righteous" media="all">
        <link href="https://fonts.googleapis.com/css?family=Ribeye Marrow" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Risque" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto Condensed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto Mono" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto Slab" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Rozha One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Rubik Mono One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ruda" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Rufina" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ruge Boogie" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ruluko" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Rum Raisin" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sarina" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sarpanch" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sawarabi Gothic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sawarabi Mincho" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Scada" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Scheherazade" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Schoolbell" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Signika Negative" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Simonetta" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Single Day" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sintony" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sirin Stencil" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Six Caps" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sniglet" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Snippet" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Snowburst One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sofadi One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sofia" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Solway" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spartan" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Special Elite" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spectral" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spectral SC" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spicy Rice" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spinnaker" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Spirax" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sumana" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sunshiney" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Supermercado One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sura" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Suranna" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Suravaram" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Suwannaphum" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=The Girl Next Door" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Tienne" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Tillana" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Timmana" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Tinos" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Titan One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Titillium Web" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Tomorrow" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Trade Winds" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sue Ellen Francisco" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Suez One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sulphur Point" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stalinist One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stardos Stencil" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stint Ultra Condensed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stint Ultra Expanded" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stoke" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Strait" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stylish" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Staatliches" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Stalemate" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Squada One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sree Krushnadevaraya" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sriracha" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Srisakdi" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Space Mono" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sonsie One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sora" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sorts Mill Goudy" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source Code Pro" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source Sans Pro" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source Serif Pro" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Song Myung" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Slackey" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Smokum" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Skranji" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Slabo 13px" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Slabo 27px" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Six+Caps" media="all">
        <link href="https://fonts.googleapis.com/css?family=Sigmar One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Share Tech" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Share Tech Mono" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Shojumaru" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Short Stack" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Shrikhand" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Siemreap" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Shadows Into Light Two" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Shanti" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Share" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sevillana" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Seymour One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Shadows Into Light" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Secular One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sedgwick Ave" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sedgwick Ave Display" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sen" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Scope One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Seaweed Script" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sedgwick+Ave+Display" media="all">
        <link href="https://fonts.googleapis.com/css?family=Sansita" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sansita Swashed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sarala" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Salsa" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sanchez" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sancreek" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sahitya" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Sail" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Saira" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Saira Condensed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Saira Extra Condensed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Saira Semi Condensed" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Saira Stencil One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ruslan Display" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Russo One" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Ruthie" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ruge+Boogie" media="all">
        <link href="https://fonts.googleapis.com/css?family=Rye" rel="stylesheet" type="text/css"></head>
    </head>
    <body class="antialiased">
        <div id="app" class="bg-gray-200 h-auto min-vh-100">
            <div class="container vw-75 pt-4 bg-white min-vh-100">
                <router-view></router-view>
            </div>
        </div>
        <script>
            window.base_url = '{{ env('APP_URL') }}/';
            window.loggedIn = '{{ $loggedIn }}';
            window.username = '{{ $loggedInUser["username"] }}';
            window.avatar = '{{ $loggedInUser["avatar"] }}';
            window.alertMessage = '{{ $message }}';
            window.alertClass = '{{ $alertClass }}';
        </script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
