/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import {DateTime} from "luxon";

require('./bootstrap');

window.Vue = require('vue').default;

import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from "vue-axios";
import Vuetify from "vuetify"
import 'vuetify/dist/vuetify.min.css'
import * as Sentry from "@sentry/vue"

Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.use(Vuetify)

const axiosConfig = {
    baseURL: window.base_url
}

axios.defaults.withCredentials = true;

Vue.prototype.$axios = axios.create(axiosConfig);
Vue.prototype.$DateTime = DateTime;
Vue.prototype.baseURL = window.base_url;

import { ValidationProvider, ValidationObserver } from 'vee-validate'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

import { library } from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import {faXmark, faAlignLeft, faAlignCenter, faAlignRight, faAlignJustify, faBold, faItalic, faTrashCan} from "@fortawesome/free-solid-svg-icons";

library.add(faTwitch, faXmark, faAlignLeft, faAlignCenter, faAlignRight, faAlignJustify, faBold, faItalic, faTrashCan);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD MMMM YY');
    }
})

import {routes} from './routes';

const router = new VueRouter({
    routes,
    mode: 'history',
})

Sentry.init({
    Vue,
    dsn: "https://126bd9bb073b467ea677759b7f5a5d06@o4504188734472192.ingest.sentry.io/4504701456482304",
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    router
});

app.$axios.interceptors.request.use(function (config) {
    config.metadata = { startTime: new Date()}
    return config;
}, function (error) {
    return Promise.reject(error);
})

app.$axios.interceptors.response.use(function (response) {
    response.config.metadata.endTime = new Date()
    response.duration = response.config.metadata.endTime - response.config.metadata.startTime
    return response;
}, function (error) {
    error.config.metadata.endTime = new Date();
    error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
    return Promise.reject(error);
})

