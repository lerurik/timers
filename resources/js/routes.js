import Home from "./components/Home";
import TimerRender from "./components/TimerRender";

export const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/render/:stub',
        component: TimerRender,
        name: 'render',
        props: {
            testMode: false
        }
    }
]
