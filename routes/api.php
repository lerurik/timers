<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('show/{Timer}', 'App\Http\Controllers\TimersController@show')->name('app.timers.show');

Route::middleware('auth:sanctum')->group(function() {
    Route::get('list', 'App\Http\Controllers\TimersController@list')->name('api.timers.list');
    Route::post('create', 'App\Http\Controllers\TimersController@create')->name('api.timers.create');
    Route::put('update/{Timer}', 'App\Http\Controllers\TimersController@update')->name('api.timers.create');
    Route::delete('delete/{Timer}', 'App\Http\Controllers\TimersController@delete')->name('api.timers.delete');
});
