<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('unpause/{Timer}', 'App\Http\Controllers\TimersController@unpause')->name('app.timers.unpause');
Route::get('pause/{Timer}', 'App\Http\Controllers\TimersController@pause')->name('app.timers.pause');
Route::get('reset/{Timer}', 'App\Http\Controllers\TimersController@reset')->name('app.timers.reset');
Route::get('add/{Timer}', 'App\Http\Controllers\TimersController@add')->name('app.timers.add');
Route::get('subtract/{Timer}', 'App\Http\Controllers\TimersController@subtract')->name('app.timers.subtract');

Route::get('/authentication/twitch/redirect', 'App\Http\Controllers\AuthenticationController@redirectToTwitch')->name('app.twitch.redirect');
Route::get('/authentication/twitch/callback', 'App\Http\Controllers\AuthenticationController@handleTwitchCallback')->name('app.twitch.callback');
Route::get('/authentication/logout', 'App\Http\Controllers\AuthenticationController@logout')->name('app.logout');

Route::get('render/{Timer}', function() {
    return view('render');
});

Route::fallback(function () {
    return view('welcome', [
        'loggedIn' => Auth::check(),
        'loggedInUser' => [
            'username' => Auth::check() ? Auth::user()->name : "",
            'avatar' => Auth::check() ? Auth::user()->avatar : "",
        ],
        'message' => Session::has('message') ? Session::get('message') : '',
        'alertClass' => Session::has('alert-class') ? Session::get('alert-class') : '',
    ]);
});
