/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    './resources/js/components/*.vue',
    './resources/views/welcome.blade.php',
  ],
  theme: {
    extend: {
      borderRadius: {
        '5xl': '1.25em !important',
      }
    }
  },
  plugins: [],
}
