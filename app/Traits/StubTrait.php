<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;

trait StubTrait
{
    protected static function booted()
    {
        static::saving(function ($model) {
            if (Schema::hasColumn($model->table, 'stub')) {
                if (empty($model->stub)) {
                    $stubInUse = true;
                    $stub = '';
                    $characters = '0123456789abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    while ($stubInUse) {
                        list($usec, $sec) = explode(' ', microtime());
                        srand($sec + ((float) $usec * 100000));
                        for ($i = 0; $i < 10; $i++) {
                            $stub .= $characters[rand(0, $charactersLength - 1)];
                        }
                        $existingStub = self::get()->where('stub', $stub)->first();
                        if (is_null($existingStub)) {
                            $stubInUse = false;
                        } else {
                            $stub = '';
                        }
                        $model->stub = $stub;
                    }
                }
            }
        });
    }
}
