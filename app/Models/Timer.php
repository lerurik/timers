<?php

namespace App\Models;

use App\Traits\StubTrait;
use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    use StubTrait;

    protected $table = 'timers';

    protected $fillable = [
        "name",
        "width",
        "height",
        "fontFamily",
        "fontColour",
        "fontSize",
        "lineHeight",
        "alignment",
        "fontWeight",
        "italic",
        "textStrokeWidth",
        "textStrokeColour",
        "textShadow",
        "textShadowColour",
        "textShadowX",
        "textShadowY",
        "textShadowBlur",
        "initialTime",
        "startingTime",
        "additionalTime",
        "format",
        "showEndText",
        "endText",
        "stub",
        'paused',
        'pausedTime',
        "UserID",
    ];

    public function toArray()
    {
        return [
            "name" => $this->name,
            "width" => $this->width,
            "height" => $this->height,
            "fontFamily" => $this->fontFamily,
            "fontColour" => $this->fontColour,
            "fontSize" => $this->fontSize,
            "lineHeight" => $this->lineHeight,
            "alignment" => $this->alignment,
            "fontWeight" => $this->fontWeight,
            "italic" => $this->italic,
            "textStrokeWidth" => $this->textStrokeWidth,
            "textStrokeColour" => $this->textStrokeColour,
            "textShadow" => $this->textShadow,
            "textShadowColour" => $this->textShadowColour,
            "textShadowX" => $this->textShadowX,
            "textShadowY" => $this->textShadowY,
            "textShadowBlur" => $this->textShadowBlur,
            "initialTime" => $this->initialTime,
            "startingTime" => $this->startingTime,
            "additionalTime" => $this->additionalTime,
            "format" => $this->format,
            "showEndText" => $this->showEndText,
            "endText" => $this->endText,
            'stub' => $this->stub,
            'paused' => $this->paused,
            'pausedTime' => $this->pausedTime,
        ];
    }
}
