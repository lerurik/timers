<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class AuthenticationController extends Controller
{
    public function redirectToTwitch()
    {
        return Socialite::driver('twitch')
            ->redirect();
    }

    public function handleTwitchCallback()
    {
        try {
            $twitchUser = Socialite::driver('twitch')->user();

            $user = User::firstOrCreate([
                'email' => $twitchUser->email,
            ]);
            $user->update([
                'name' => $twitchUser->getName(),
                'avatar' => $twitchUser->getAvatar(),
            ]);
            $user->save();
            Auth::login($user);
            return redirect()->to('/');
        } catch (ClientException $e) {
            Session::flash('message', 'There was an issue authenticating with Twitch. The developer have been notified, please try again later.');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->to('/');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('/');
    }
}
