<?php

namespace App\Http\Controllers;

use App\Models\Timer;
use Illuminate\Support\Facades\Auth;

class TimersController extends Controller
{
    public function create()
    {
        $timer = new Timer([
            "name" => "New Timer",
            "UserID" => Auth::user()->id,
        ]);
        $timer->save();

        return $timer;
    }

    public function update(string $timerStub)
    {
        $timer = Timer::where([
            'stub' => $timerStub
        ])->firstOrFail();

        try {
            $timer->update(request()->all());
            $timer->save();
        } catch (\Exception $e) {
            throw new \Exception("Something went wrong updating your timer.", 500);
        }

        return $timer;
    }

    public function list()
    {
        return Timer::where('UserID', Auth::user()->id)->orderBy('id', 'asc')->get();
    }

    public function show(string $timerStub)
    {
        return Timer::where('stub', $timerStub)->first();
    }

    public function delete(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();

        if (!is_null($timer)) {
            $timer->delete();
        }

        return [
            'success' => true,
        ];
    }

    public function reset(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();

        if (!is_null($timer)) {
            $timer->update([
                'startingTime' => time(),
                'additionalTime' => 0,
                'paused' => false,
                'pausedTime' => 0,
            ]);
        }
    }

    public function add(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();
        $additionalTime = request()->get('t');

        if (!is_null($timer)) {
            $additionalTime = $timer->additionalTime + $additionalTime;
            $timer->update([
                'additionalTime' => $additionalTime,
            ]);
        }
    }

    public function subtract(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();
        $additionalTime = request()->get('t');

        if (!is_null($timer)) {
            $additionalTime = $timer->additionalTime - $additionalTime;
            $timer->update([
                'additionalTime' => $additionalTime,
            ]);
        }
    }

    public function pause(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();
        if (!is_null($timer) && !$timer->paused) {
            $timer->update([
                'paused' => true,
                'pausedTime' => time(),
            ]);
        }
    }

    public function unpause(string $timerStub)
    {
        $timer = Timer::where('stub', $timerStub)->first();
        if (!is_null($timer) && $timer->paused) {
            $timePaused = time() - $timer->pausedTime;
            $additionalTime = $timer->additionalTime + $timePaused;
            $timer->update([
                'paused' => false,
                'pausedTime' => 0,
                'additionalTime' => $additionalTime,
            ]);
        }
    }
}
